/*
 * Copyright 2020  Linus Jahn <lnj@kaidan.im>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.12 as Controls
import org.kde.kirigami 2.8 as Kirigami
import org.kde.xdgdesktopportal 0.1

Kirigami.OverlayDrawer {
    id: root

    signal placeOpenRequested(url place)

    handleClosedIcon.source: null
    handleOpenIcon.source: null
    width: Math.min(applicationWindow().width * 0.8, Kirigami.Units.gridUnit * 20)

    leftPadding: 0
    rightPadding: 0

    contentItem: ListView {
        spacing: 0
        model: FilePlacesModel {
            id: filePlacesModel
        }

        section.property: "group"
        section.delegate: Kirigami.Heading {
            leftPadding: Kirigami.Units.smallSpacing
            level: 6
            text: section
        }

        delegate: Kirigami.BasicListItem {
            visible: !model.hidden
            width: parent.width
            text: model.display
            icon: model.iconName
            separatorVisible: false
            onClicked: {
                root.placeOpenRequested(model.url)
            }
        }
    }
}
