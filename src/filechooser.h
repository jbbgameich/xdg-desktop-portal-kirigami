﻿/*
 * Copyright © 2016-2018 Red Hat, Inc
 * Copyright © 2019 Linus Jahn <lnj@kaidan.im>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see <http://www.gnu.org/licenses/>.
 *
 * Authors:
 *       Jan Grulich <jgrulich@redhat.com>
 */

#ifndef XDG_DESKTOP_PORTAL_KDE_FILECHOOSER_H
#define XDG_DESKTOP_PORTAL_KDE_FILECHOOSER_H

#include <QDBusObjectPath>
#include <QDBusAbstractAdaptor>
#include <QQmlApplicationEngine>

class FileDialog : public QObject
{
    Q_OBJECT
public:
    friend class FileChooserPortal;

    FileDialog();
    ~FileDialog();

protected:
    QQmlApplicationEngine *m_engine;
};

class FileChooserQmlCallback : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString title READ title WRITE setTitle NOTIFY titleChanged)
    Q_PROPERTY(bool selectMultiple READ selectMultiple WRITE setSelectMultiple NOTIFY selectMultipleChanged)
    Q_PROPERTY(bool selectExisting READ selectExisting WRITE setSelectExisting NOTIFY selectExistingChanged)
    Q_PROPERTY(QStringList nameFilters READ nameFilters WRITE setNameFilters NOTIFY nameFiltersChanged)
    Q_PROPERTY(QStringList mimeTypeFilters READ mimeTypeFilters WRITE setMimeTypeFilters NOTIFY mimeTypeFiltersChanged)
    Q_PROPERTY(QString folder READ folder WRITE setFolder NOTIFY folderChanged)
    Q_PROPERTY(QString currentFile READ currentFile WRITE setCurrentFile NOTIFY currentFileChanged)
    Q_PROPERTY(QString acceptLabel READ acceptLabel WRITE setAcceptLabel NOTIFY acceptLabelChanged)

public:
    FileChooserQmlCallback(QObject *parent = nullptr);

    QString title() const;
    void setTitle(const QString &title);

    bool selectMultiple() const;
    void setSelectMultiple(bool selectMultiple);

    bool selectExisting() const;
    void setSelectExisting(bool selectExisting);

    QStringList nameFilters() const;
    void setNameFilters(const QStringList &nameFilters);

    QStringList mimeTypeFilters() const;
    void setMimeTypeFilters(const QStringList &mimeTypeFilters);

    QString folder() const;
    void setFolder(const QString &folder);

    QString currentFile() const;
    void setCurrentFile(const QString &currentFile);

    QString acceptLabel() const;
    void setAcceptLabel(const QString &acceptLabel);

Q_SIGNALS:
    void accepted(const QStringList &files);
    void titleChanged();
    void selectMultipleChanged();
    void selectExistingChanged();
    void nameFiltersChanged();
    void mimeTypeFiltersChanged();
    void folderChanged();
    void currentFileChanged();
    void acceptLabelChanged();

private:
    QString m_title;
    bool m_selectMultiple;
    bool m_selectExisting;
    QStringList m_nameFilters;
    QStringList m_mimeTypeFilters;
    QString m_folder;
    QString m_currentFile;
    QString m_acceptLabel;
};

class FileChooserPortal : public QDBusAbstractAdaptor
{
    Q_OBJECT
    Q_CLASSINFO("D-Bus Interface", "org.freedesktop.impl.portal.FileChooser")
public:
    // Keep in sync with qflatpakfiledialog from flatpak-platform-plugin
    typedef struct {
        uint type;
        QString filterString;
    } Filter;
    typedef QList<Filter> Filters;

    typedef struct {
        QString userVisibleName;
        Filters filters;
    } FilterList;
    typedef QList<FilterList> FilterListList;

    explicit FileChooserPortal(QObject *parent);
    ~FileChooserPortal();

public Q_SLOTS:
    uint OpenFile(const QDBusObjectPath &handle,
                  const QString &app_id,
                  const QString &parent_window,
                  const QString &title,
                  const QVariantMap &options,
                  QVariantMap &results);

    uint SaveFile(const QDBusObjectPath &handle,
                  const QString &app_id,
                  const QString &parent_window,
                  const QString &title,
                  const QVariantMap &options,
                  QVariantMap &results);
};

#endif // XDG_DESKTOP_PORTAL_KDE_FILECHOOSER_H
