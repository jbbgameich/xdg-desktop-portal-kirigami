/*
 * Copyright © 2016-2019 Red Hat, Inc
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see <http://www.gnu.org/licenses/>.
 *
 * Authors:
 *       Jan Grulich <jgrulich@redhat.com>
 */

#ifndef XDG_DESKTOP_PORTAL_KDE_APPCHOOSER_DIALOG_H
#define XDG_DESKTOP_PORTAL_KDE_APPCHOOSER_DIALOG_H

#include <QQmlApplicationEngine>

#include <QAbstractListModel>
#include <QSortFilterProxyModel>

#include "appmodel.h"

class AppChooserQmlCallback : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString defaultApp READ defaultApp WRITE setDefaultApp NOTIFY defaultAppChanged)
    //Q_PROPERTY(QString selectedApp READ selectedApp WRITE setSelectedApp NOTIFY selectedAppChanged)
    Q_PROPERTY(QString fileName READ fileName WRITE setFileName NOTIFY fileNameChanged)
    Q_PROPERTY(AppModel* appModel READ appModel CONSTANT)

public:
    explicit AppChooserQmlCallback(QObject *parent = nullptr);

    QString defaultApp() const;
    void setDefaultApp(const QString &defaultApp);

    QString selectedApplication() const;
    void setSelectedApplication(const QString &selectedApplication);

    QString fileName() const;
    void setFileName(const QString &fileName);

    AppModel* appModel() const;

private:
    AppModel* m_appModel = nullptr;

    QString m_defaultApp;
    //QString m_selectedApp;
    QString m_fileName;

Q_SIGNALS:
    void applicationSelected(const QString &desktopFile);
    void openDiscover();

    void defaultAppChanged();
    void fileNameChanged();
};


class AppChooserDialog : public QObject
{
    Q_OBJECT
public:
    explicit AppChooserDialog(const QStringList &choices, const QString &defaultApp, const QString &fileName, QObject *parent = nullptr, Qt::WindowFlags flags = {});
    ~AppChooserDialog();

    void updateChoices(const QStringList &choices);

    QString selectedApplication() const;

private Q_SLOTS:
    void onOpenDiscover();

private:
    QQmlApplicationEngine m_engine;
    AppChooserQmlCallback* m_callback;

    QStringList m_defaultChoices;
    QString m_defaultApp;

Q_SIGNALS:
    void accepted(const QString &desktopFile);
};

#endif // XDG_DESKTOP_PORTAL_KDE_APPCHOOSER_DIALOG_H
