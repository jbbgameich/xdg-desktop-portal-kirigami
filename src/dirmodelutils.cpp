/*
 * Copyright 2019  Linus Jahn <lnj@kaidan.im>
 * Copyright 2019  Jonah Brüchert <jbb@kaidan.im>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "dirmodelutils.h"
#include <QDebug>
#include <QUrl>
#include <QStandardPaths>

DirModelUtils::DirModelUtils(QObject *parent) : QObject(parent)
{
}

QStringList DirModelUtils::getUrlParts(const QUrl &url) const
{
    if (url.path() == QStringLiteral("/"))
        return QStringList();
    return url.path().split(QStringLiteral("/")).mid(1);
}

QUrl DirModelUtils::indexOfUrl(const QUrl &url, int index) const
{
    const QStringList urlParts = url.path().split(QStringLiteral("/"));
    QString path = QStringLiteral("/");
    for (int i = 0; i < index + 1; i++) {
        path += urlParts.at(i + 1);
        path += QStringLiteral("/");
    }

    qDebug() << path;
    return QUrl::fromLocalFile(path);
}

QString DirModelUtils::homePath() const
{
    QUrl url(QStandardPaths::writableLocation(QStandardPaths::HomeLocation));
    url.setScheme(QStringLiteral("file"));

    return url.toString();
}
