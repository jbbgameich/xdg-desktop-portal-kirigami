/*
 * Copyright 2019  Linus Jahn <lnj@kaidan.im>
 * Copyright 2019  Jonah Brüchert <jbb@kaidan.im>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DIRMODEL_H
#define DIRMODEL_H

#include <QAbstractListModel>
#include <QVariant>
#include <KIOCore/KFileItem>
class KCoreDirLister;
class KFileItemList;

class DirModel : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(const QUrl& folder READ folder WRITE setFolder NOTIFY folderChanged)
    Q_PROPERTY(bool showDotFiles READ showDotFiles WRITE setShowDotFiles NOTIFY showDotFilesChanged)
    Q_PROPERTY(bool isLoading READ isLoading NOTIFY isLoadingChanged)
    Q_PROPERTY(QString nameFilter READ nameFilter NOTIFY nameFilterChanged)
    Q_PROPERTY(QStringList mimeFilters READ mimeFilters NOTIFY mimeFiltersChanged)

public:
    enum Roles {
        Name = Qt::UserRole + 1,
        Url,
        IconName,
        IsDir,
        IsLink,
        FileSize,
        MimeType,
        IsHidden,
        IsReadable,
        IsWritable,
        ModificationTime
    };

    Q_ENUM(Roles)

    explicit DirModel(QObject *parent = nullptr);

    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    QHash<int, QByteArray> roleNames() const override;

    QUrl folder() const;
    void setFolder(const QUrl &folder);

    bool showDotFiles() const;
    void setShowDotFiles(bool showDotFiles);

    bool isLoading() const;
    void setIsLoading(bool isLoading);

    QString nameFilter() const;
    void setNameFilter(const QString &nameFilter);

    QStringList mimeFilters() const;
    void setMimeFilters(const QStringList &mimeFilters);

Q_SIGNALS:
    void folderChanged();
    void showDotFilesChanged();
    void isLoadingChanged();
    void nameFilterChanged();
    void mimeFiltersChanged();

private Q_SLOTS:
    void handleCompleted();
    void handleNewItems(const KFileItemList &items);
    void handleItemsDeleted(const KFileItemList &items);
    void handleRedirection(const QUrl &oldUrl, const QUrl &newUrl);

private:
    KCoreDirLister *m_lister;
    QVector<KFileItem> m_items;

    QUrl m_folder;
    bool m_showDotFiles;
    bool m_isLoading;
    QString m_nameFilter;
    QStringList m_mimeFilters;
};

#endif // DIRMODEL_H
