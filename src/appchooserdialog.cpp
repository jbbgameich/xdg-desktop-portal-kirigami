/*
 * Copyright © 2017-2019 Red Hat, Inc
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see <http://www.gnu.org/licenses/>.
 *
 * Authors:
 *       Jan Grulich <jgrulich@redhat.com>
 */

#include "appchooserdialog.h"

#include <QQmlContext>
#include <QQuickWindow>
#include <QQmlApplicationEngine>

#include <QDir>
#include <QStandardPaths>
#include <QSettings>
#include <KProcess>

#include <QDebug>

AppChooserDialog::AppChooserDialog(const QStringList &choices, const QString &defaultApp, const QString &fileName, QObject *parent, Qt::WindowFlags flags)
    : QObject(parent)
    , m_engine(new QQmlApplicationEngine)
    , m_defaultChoices(choices)
    , m_defaultApp(defaultApp)
{
    qmlRegisterUncreatableType<AppModel>("org.kde.xdgdesktopportal", 1, 0, "AppModel", QStringLiteral("Created by AppChooserCallback"));
    qmlRegisterType<AppFilterModel>("org.kde.xdgdesktopportal", 1, 0, "AppFilterModel");
    qmlRegisterType<AppChooserQmlCallback>("org.kde.xdgdesktopportal", 1, 0, "AppChooserCallback");

    m_engine.load(QStringLiteral(":/AppChooserDialog.qml"));

    auto* window = qobject_cast<QQuickWindow*>(m_engine.rootObjects().first());
    window->setFlags(flags);

    m_callback = m_engine.rootObjects().first()->findChild<AppChooserQmlCallback*>({}, Qt::FindChildrenRecursively);
    m_callback->setDefaultApp(defaultApp);
    m_callback->setFileName(fileName);
    m_callback->appModel()->setPreferredApps(choices);

    connect(m_callback, &AppChooserQmlCallback::openDiscover, this, &AppChooserDialog::onOpenDiscover);

    // Forward signal to the public
    connect(m_callback, &AppChooserQmlCallback::applicationSelected, this, &AppChooserDialog::accepted);
}

AppChooserDialog::~AppChooserDialog()
{
}

void AppChooserDialog::onOpenDiscover()
{
    KProcess::startDetached(QStringLiteral("plasma-discover"));
}

AppModel* AppChooserQmlCallback::appModel() const
{
    return m_appModel;
}

QString AppChooserQmlCallback::fileName() const
{
    return m_fileName;
}

void AppChooserQmlCallback::setFileName(const QString &fileName)
{
    m_fileName = fileName;
}

AppChooserQmlCallback::AppChooserQmlCallback(QObject *parent)
    : QObject(parent)
    , m_appModel(new AppModel(this))
{
}

QString AppChooserQmlCallback::defaultApp() const
{
    return m_defaultApp;
}

void AppChooserQmlCallback::setDefaultApp(const QString &defaultApp)
{
    m_defaultApp = defaultApp;
}

void AppChooserDialog::updateChoices(const QStringList &choices)
{
    m_callback->appModel()->setPreferredApps(choices);
}
