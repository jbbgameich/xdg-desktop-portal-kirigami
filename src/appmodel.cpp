#include "appmodel.h"

#include <QDir>
#include <QSettings>
#include <QStandardPaths>

ApplicationItem::ApplicationItem(const QString &name, const QString &icon, const QString &desktopFileName)
    : m_applicationName(name)
    , m_applicationIcon(icon)
    , m_applicationDesktopFile(desktopFileName)
    , m_applicationCategory(AllApplications)
{
}

QString ApplicationItem::applicationName() const
{
    return m_applicationName;
}

QString ApplicationItem::applicationIcon() const
{
    return m_applicationIcon;
}

QString ApplicationItem::applicationDesktopFile() const
{
    return m_applicationDesktopFile;
}

void ApplicationItem::setApplicationCategory(ApplicationItem::ApplicationCategory category)
{
    m_applicationCategory = category;
}

ApplicationItem::ApplicationCategory ApplicationItem::applicationCategory() const
{
    return m_applicationCategory;
}

bool ApplicationItem::operator==(const ApplicationItem &item) const
{
    return item.applicationDesktopFile() == applicationDesktopFile();
}

AppFilterModel::AppFilterModel(QObject *parent)
    : QSortFilterProxyModel(parent)
{
    setDynamicSortFilter(true);
    setFilterCaseSensitivity(Qt::CaseInsensitive);
    sort(0, Qt::DescendingOrder);
}

AppFilterModel::~AppFilterModel()
{
}

void AppFilterModel::setShowOnlyPrefferedApps(bool show)
{
    m_showOnlyPreferredApps = show;

    invalidate();
}

bool AppFilterModel::showOnlyPreferredApps() const
{
    return m_showOnlyPreferredApps;
}

void AppFilterModel::setFilter(const QString &text)
{
    m_filter = text;

    invalidate();
}

QString AppFilterModel::filter() const
{
    return m_filter;
}

bool AppFilterModel::filterAcceptsRow(int source_row, const QModelIndex &source_parent) const
{
    const QModelIndex index = sourceModel()->index(source_row, 0, source_parent);

    ApplicationItem::ApplicationCategory category = static_cast<ApplicationItem::ApplicationCategory>(sourceModel()->data(index, AppModel::ApplicationCategoryRole).toInt());
    QString appName = sourceModel()->data(index, AppModel::ApplicationNameRole).toString();

    if (m_showOnlyPreferredApps)
        return category == ApplicationItem::PreferredApplication;

    if (category == ApplicationItem::PreferredApplication)
        return true;

    if (m_filter.isEmpty())
        return true;

    return appName.toLower().contains(m_filter);
}

bool AppFilterModel::lessThan(const QModelIndex &left, const QModelIndex &right) const
{
    ApplicationItem::ApplicationCategory leftCategory = static_cast<ApplicationItem::ApplicationCategory>(sourceModel()->data(left, AppModel::ApplicationCategoryRole).toInt());
    ApplicationItem::ApplicationCategory rightCategory = static_cast<ApplicationItem::ApplicationCategory>(sourceModel()->data(right, AppModel::ApplicationCategoryRole).toInt());
    QString leftName = sourceModel()->data(left, AppModel::ApplicationNameRole).toString();
    QString rightName = sourceModel()->data(right, AppModel::ApplicationNameRole).toString();

    if (leftCategory < rightCategory) {
        return false;
    } else if (leftCategory > rightCategory) {
        return true;
    }

    return QString::localeAwareCompare(leftName, rightName) > 0;
}

AppModel::AppModel(QObject *parent)
    : QAbstractListModel(parent)
{
    loadApplications();
}

AppModel::~AppModel()
{
}

void AppModel::setPreferredApps(const QStringList &list)
{
    for (ApplicationItem &item : m_list) {
        bool changed = false;

        // First reset to initial type
        if (item.applicationCategory() != ApplicationItem::AllApplications) {
            item.setApplicationCategory(ApplicationItem::AllApplications);
            changed = true;
        }

        if (list.contains(item.applicationDesktopFile())) {
            item.setApplicationCategory(ApplicationItem::PreferredApplication);
            changed = true;
        }

        if (changed) {
            const int row = m_list.indexOf(item);
            if (row >= 0) {
                QModelIndex index = createIndex(row, 0, AppModel::ApplicationCategoryRole);
                Q_EMIT dataChanged(index, index);
            }
        }
    }
}

QVariant AppModel::data(const QModelIndex &index, int role) const
{
    const int row = index.row();

    if (row >= 0 && row < m_list.count()) {
        ApplicationItem item = m_list.at(row);

        switch (role) {
            case ApplicationNameRole:
                return item.applicationName();
            case ApplicationIconRole:
                return item.applicationIcon();
            case ApplicationDesktopFileRole:
                return item.applicationDesktopFile();
            case ApplicationCategoryRole:
                return static_cast<int>(item.applicationCategory());
            default:
                break;
        }
    }

    return QVariant();
}

int AppModel::rowCount(const QModelIndex &parent) const
{
    return parent.isValid() ? 0 : m_list.count();
}

QHash<int, QByteArray> AppModel::roleNames() const
{
    QHash<int, QByteArray> roles = QAbstractListModel::roleNames();
    roles[ApplicationNameRole] = "ApplicationName";
    roles[ApplicationIconRole] = "ApplicationIcon";
    roles[ApplicationDesktopFileRole] = "ApplicationDesktopFile";
    roles[ApplicationCategoryRole] = "ApplicationCategory";

    return roles;
}

void AppModel::loadApplications()
{
    for (const QString &location : QStandardPaths::standardLocations(QStandardPaths::ApplicationsLocation)) {
        QDir dir(location);
        for (QString &entry : dir.entryList(QStringList({QStringLiteral("*.desktop")}), QDir::Files, QDir::Name)) {
            QString applicationIcon;
            QString applicationName;

            QSettings settings(QStringLiteral("%1/%2").arg(dir.path()).arg(entry), QSettings::IniFormat);
            settings.beginGroup(QStringLiteral("Desktop Entry"));
            if (settings.contains(QStringLiteral("X-GNOME-FullName"))) {
                applicationName = settings.value(QStringLiteral("X-GNOME-FullName")).toString();
            } else {
                applicationName = settings.value(QStringLiteral("Name")).toString();
            }
            applicationIcon = settings.value(QStringLiteral("Icon")).toString();

            const QString desktopFileWithoutSuffix = entry.remove(QStringLiteral(".desktop"));
            if (applicationName.isEmpty() || applicationIcon.isEmpty())
                continue;

            ApplicationItem appItem(applicationName, applicationIcon, desktopFileWithoutSuffix);

            if (!m_list.contains(appItem))
                m_list.append(appItem);
        }
    }
}
