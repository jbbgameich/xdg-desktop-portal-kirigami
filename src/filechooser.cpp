/*
 * Copyright © 2016-2018 Red Hat, Inc
 * Copyright © 2019 Linus Jahn <lnj@kaidan.im>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see <http://www.gnu.org/licenses/>.
 *
 * Authors:
 *       Jan Grulich <jgrulich@redhat.com>
 */

#include "filechooser.h"
#include "utils.h"
#include "dirmodel.h"
#include "dirmodelutils.h"
#include "fileplacesmodel.h"

#include <QDialogButtonBox>
#include <QDBusMetaType>
#include <QDBusArgument>
#include <QLoggingCategory>
#include <QFile>
#include <QPushButton>
#include <QVBoxLayout>
#include <QUrl>
#include <QQmlEngine>
#include <QQmlContext>
#include <QApplication>

#include <KLocalizedString>

Q_LOGGING_CATEGORY(XdgDesktopPortalKirigamiFileChooser, "xdp-kde-file-chooser")

// Keep in sync with qflatpakfiledialog from flatpak-platform-plugin
Q_DECLARE_METATYPE(FileChooserPortal::Filter)
Q_DECLARE_METATYPE(FileChooserPortal::Filters)
Q_DECLARE_METATYPE(FileChooserPortal::FilterList)
Q_DECLARE_METATYPE(FileChooserPortal::FilterListList)

QDBusArgument &operator << (QDBusArgument &arg, const FileChooserPortal::Filter &filter)
{
    arg.beginStructure();
    arg << filter.type << filter.filterString;
    arg.endStructure();
    return arg;
}

const QDBusArgument &operator >> (const QDBusArgument &arg, FileChooserPortal::Filter &filter)
{
    uint type;
    QString filterString;
    arg.beginStructure();
    arg >> type >> filterString;
    filter.type = type;
    filter.filterString = filterString;
    arg.endStructure();

    return arg;
}

QDBusArgument &operator << (QDBusArgument &arg, const FileChooserPortal::FilterList &filterList)
{
    arg.beginStructure();
    arg << filterList.userVisibleName << filterList.filters;
    arg.endStructure();
    return arg;
}

const QDBusArgument &operator >> (const QDBusArgument &arg, FileChooserPortal::FilterList &filterList)
{
    QString userVisibleName;
    FileChooserPortal::Filters filters;
    arg.beginStructure();
    arg >> userVisibleName >> filters;
    filterList.userVisibleName = userVisibleName;
    filterList.filters = filters;
    arg.endStructure();

    return arg;
}

FileDialog::FileDialog()
    : QObject()
    , m_engine(new QQmlApplicationEngine)
{
    // Register QML Types
    qmlRegisterType<DirModel>("org.kde.xdgdesktopportal", 0, 1, "DirModel");
    qmlRegisterType<DirModelUtils>("org.kde.xdgdesktopportal", 0, 1, "DirModelUtils");
    qmlRegisterType<FileChooserQmlCallback>("org.kde.xdgdesktopportal", 0, 1, "FileChooserCallback");
    qmlRegisterType<FilePlacesModel>("org.kde.xdgdesktopportal", 0, 1, "FilePlacesModel");

    m_engine->load(QStringLiteral("qrc:/FileChooser.qml"));
}

FileDialog::~FileDialog()
{
	m_engine->deleteLater();
}

FileChooserQmlCallback::FileChooserQmlCallback(QObject *parent)
    : QObject(parent),
      m_selectMultiple(true),
      m_selectExisting(true),
      m_nameFilters(QStringList() << QStringLiteral("*")),
      m_mimeTypeFilters(QStringList() << QStringLiteral("*/*"))
{
}

QString FileChooserQmlCallback::title() const
{
    return m_title;
}

void FileChooserQmlCallback::setTitle(const QString &title)
{
    m_title = title;
    emit titleChanged();
}

bool FileChooserQmlCallback::selectMultiple() const
{
    return m_selectMultiple;
}

void FileChooserQmlCallback::setSelectMultiple(bool selectMultiple)
{
    m_selectMultiple = selectMultiple;
    emit selectMultipleChanged();
}

bool FileChooserQmlCallback::selectExisting() const
{
    return m_selectExisting;
}

void FileChooserQmlCallback::setSelectExisting(bool selectExisting)
{
    m_selectExisting = selectExisting;
    emit selectExistingChanged();
}

QStringList FileChooserQmlCallback::nameFilters() const
{
    return m_nameFilters;
}

void FileChooserQmlCallback::setNameFilters(const QStringList &nameFilters)
{
    m_nameFilters = nameFilters;
    emit nameFiltersChanged();
}

QStringList FileChooserQmlCallback::mimeTypeFilters() const
{
    return m_mimeTypeFilters;
}

void FileChooserQmlCallback::setMimeTypeFilters(const QStringList &mimeTypeFilters)
{
    m_mimeTypeFilters = mimeTypeFilters;
    emit mimeTypeFiltersChanged();
}

QString FileChooserQmlCallback::folder() const
{
    return m_folder;
}

void FileChooserQmlCallback::setFolder(const QString &folder)
{
    m_folder = folder;
    emit folderChanged();
}

QString FileChooserQmlCallback::currentFile() const
{
    return m_currentFile;
}

void FileChooserQmlCallback::setCurrentFile(const QString &currentFile)
{
    m_currentFile = currentFile;
    emit currentFileChanged();
}

QString FileChooserQmlCallback::acceptLabel() const
{
    return m_acceptLabel;
}

void FileChooserQmlCallback::setAcceptLabel(const QString &acceptLabel)
{
    m_acceptLabel = acceptLabel;
    emit acceptLabelChanged();
}

FileChooserPortal::FileChooserPortal(QObject *parent)
    : QDBusAbstractAdaptor(parent)
{
    qDBusRegisterMetaType<Filter>();
    qDBusRegisterMetaType<Filters>();
    qDBusRegisterMetaType<FilterList>();
    qDBusRegisterMetaType<FilterListList>();
}

FileChooserPortal::~FileChooserPortal()
{
}

uint FileChooserPortal::OpenFile(const QDBusObjectPath &handle,
                           const QString &app_id,
                           const QString &parent_window,
                           const QString &title,
                           const QVariantMap &options,
                           QVariantMap &results)
{
    Q_UNUSED(app_id)

    qCDebug(XdgDesktopPortalKirigamiFileChooser) << "OpenFile called with parameters:";
    qCDebug(XdgDesktopPortalKirigamiFileChooser) << "    handle: " << handle.path();
    qCDebug(XdgDesktopPortalKirigamiFileChooser) << "    parent_window: " << parent_window;
    qCDebug(XdgDesktopPortalKirigamiFileChooser) << "    title: " << title;
    qCDebug(XdgDesktopPortalKirigamiFileChooser) << "    options: " << options;

    bool multipleFiles = false;
    QString acceptLabel;
    QStringList nameFilters;
    QStringList mimeTypeFilters;

    /* TODO
     * choices a(ssa(ss)s)
     * List of serialized combo boxes to add to the file chooser.
     *
     * For each element, the first string is an ID that will be returned with the response, te second string is a user-visible label.
     * The a(ss) is the list of choices, each being a is an ID and a user-visible label. The final string is the initial selection,
     * or "", to let the portal decide which choice will be initially selected. None of the strings, except for the initial selection, should be empty.
     *
     * As a special case, passing an empty array for the list of choices indicates a boolean choice that is typically displayed as a check button, using "true" and "false" as the choices.
     * Example: [('encoding', 'Encoding', [('utf8', 'Unicode (UTF-8)'), ('latin15', 'Western')], 'latin15'), ('reencode', 'Reencode', [], 'false')]
     */

    if (options.contains(QStringLiteral("accept_label"))) {
        acceptLabel = options.value(QStringLiteral("accept_label")).toString();
    }

    if (options.contains(QStringLiteral("multiple"))) {
        multipleFiles = options.value(QStringLiteral("multiple")).toBool();
    }

    if (options.contains(QStringLiteral("filters"))) {
        FilterListList filterListList = qdbus_cast<FilterListList>(options.value(QStringLiteral("filters")));
        for (const FilterList &filterList : qAsConst(filterListList)) {
            QStringList filterStrings;
            for (const Filter &filterStruct : filterList.filters) {
                if (filterStruct.type == 0) {
                    filterStrings << filterStruct.filterString;
                } else {
                    mimeTypeFilters << filterStruct.filterString;
                }
            }

            if (!filterStrings.isEmpty()) {
                nameFilters << QStringLiteral("%1|%2").arg(filterStrings.join(QLatin1Char(' '))).arg(filterList.userVisibleName);
            }
        }
    }

    QScopedPointer<FileDialog, QScopedPointerDeleteLater> fileDialog(new FileDialog());

    FileChooserQmlCallback *callback = fileDialog->m_engine->rootObjects().first()->findChild<FileChooserQmlCallback*>({}, Qt::FindChildrenRecursively);

    callback->setTitle(title);
    callback->setSelectMultiple(multipleFiles);
    // Always true if we want to open a file
    callback->setSelectExisting(true);
    if (!nameFilters.isEmpty())
        callback->setNameFilters(nameFilters);
    if (!mimeTypeFilters.isEmpty())
        callback->setMimeTypeFilters(mimeTypeFilters);

    bool handled = false;
    uint exitCode = 0;
    connect(callback, &FileChooserQmlCallback::accepted, this, [&results, &handled, &exitCode] (const QStringList &urls) mutable {
        QStringList files;

        for (const auto &filename : urls)
            files << QUrl(filename).toDisplayString();

        if (files.isEmpty()) {
            qCDebug(XdgDesktopPortalKirigamiFileChooser) << "Failed to open file: no local file selected";
            exitCode = 2;
        }

        results.insert(QStringLiteral("uris"), files);
        handled = true;
        exitCode = 0;
        qDebug() << "Got results" << results;
    });
    connect(fileDialog->m_engine, &QQmlApplicationEngine::quit, [&exitCode, &handled] {
        handled = true;
        exitCode = 1;
    });

    while (!handled)
        QGuiApplication::instance()->processEvents();

    return exitCode;
}

uint FileChooserPortal::SaveFile(const QDBusObjectPath &handle,
                           const QString &app_id,
                           const QString &parent_window,
                           const QString &title,
                           const QVariantMap &options,
                           QVariantMap &results)
{
    Q_UNUSED(app_id)

    qCDebug(XdgDesktopPortalKirigamiFileChooser) << "SaveFile called with parameters:";
    qCDebug(XdgDesktopPortalKirigamiFileChooser) << "    handle: " << handle.path();
    qCDebug(XdgDesktopPortalKirigamiFileChooser) << "    parent_window: " << parent_window;
    qCDebug(XdgDesktopPortalKirigamiFileChooser) << "    title: " << title;
    qCDebug(XdgDesktopPortalKirigamiFileChooser) << "    options: " << options;

    QString acceptLabel;
    QString currentName;
    QString currentFolder;
    QString currentFile;
    QStringList nameFilters;
    QStringList mimeTypeFilters;

    // TODO parse options - choices

    if (options.contains(QStringLiteral("accept_label"))) {
        acceptLabel = options.value(QStringLiteral("accept_label")).toString();
    }

    if (options.contains(QStringLiteral("current_name"))) {
        currentName = options.value(QStringLiteral("current_name")).toString();
    }

    if (options.contains(QStringLiteral("current_folder"))) {
        currentFolder = QFile::decodeName(options.value(QStringLiteral("current_folder")).toByteArray());
    }

    if (options.contains(QStringLiteral("current_file"))) {
        currentFile = QFile::decodeName(options.value(QStringLiteral("current_file")).toByteArray());
    }

    if (options.contains(QStringLiteral("filters"))) {
        FilterListList filterListList = qdbus_cast<FilterListList>(options.value(QStringLiteral("filters")));
        for (const FilterList &filterList : qAsConst(filterListList)) {
            QStringList filterStrings;
            for (const Filter &filterStruct : filterList.filters) {
                if (filterStruct.type == 0) {
                    filterStrings << filterStruct.filterString;
                } else {
                    mimeTypeFilters << filterStruct.filterString;
                }
            }

            if (!filterStrings.isEmpty()) {
                nameFilters << QStringLiteral("%1|%2").arg(filterStrings.join(QLatin1Char(' '))).arg(filterList.userVisibleName);
            }
        }
    }

    QScopedPointer<FileDialog, QScopedPointerDeleteLater> fileDialog(new FileDialog());

    // Find the created object on QML side
    // Our qml file only has one rootObject
    auto *callback = fileDialog->m_engine->rootObjects().first()->findChild<FileChooserQmlCallback*>({}, Qt::FindChildrenRecursively);


	callback->setTitle(title);
	// Always false if we want to save a file
	callback->setSelectExisting(false);
	if (!nameFilters.isEmpty())
		callback->setNameFilters(nameFilters);
	if (!mimeTypeFilters.isEmpty())
		callback->setMimeTypeFilters(mimeTypeFilters);

    if (!currentFolder.isEmpty()) {
        callback->setFolder(currentFolder);
    }

    if (!currentFile.isEmpty()) {
        callback->setCurrentFile(currentFile);
    }

    if (!currentName.isEmpty()) {
        const auto url = QUrl(callback->folder());
        callback->setCurrentFile(
                    QUrl::fromLocalFile(QStringLiteral("%1/%2").arg(url.toDisplayString(QUrl::StripTrailingSlash), currentName)).toString());
    }

    if (!acceptLabel.isEmpty()) {
        callback->setAcceptLabel(acceptLabel);
    }

    if (!nameFilters.isEmpty()) {
        callback->setNameFilters(nameFilters);
    }

    if (!mimeTypeFilters.isEmpty()) {
        callback->setMimeTypeFilters(QStringList(mimeTypeFilters));
    }


    bool handled = false;
    uint exitCode = 0;
    connect(callback, &FileChooserQmlCallback::accepted, this, [&exitCode, &results, &handled] (const QStringList &urls) {
        QStringList files;
        for (const auto &filename : urls)
            files << QUrl(filename).toDisplayString();

        results.insert(QStringLiteral("uris"), files);
        handled = true;
        exitCode = 0;
        qDebug() << "Got results" << results;
    });
    connect(fileDialog->m_engine, &QQmlApplicationEngine::quit, [&exitCode, &handled] {
        handled = true;
        exitCode = 1;
    });

    while (!handled)
        QGuiApplication::instance()->processEvents();

    return exitCode;
}
