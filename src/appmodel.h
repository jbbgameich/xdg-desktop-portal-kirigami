#ifndef APPMODEL_H
#define APPMODEL_H

#include <QObject>
#include <QSortFilterProxyModel>

class ApplicationItem
{
public:
    enum ApplicationCategory {
        PreferredApplication,
        AllApplications
    };

    explicit ApplicationItem(const QString &name, const QString &icon, const QString &desktopFileName);

    QString applicationName() const;
    QString applicationIcon() const;
    QString applicationDesktopFile() const;

    void setApplicationCategory(ApplicationCategory category);
    ApplicationCategory applicationCategory() const;

    bool operator==(const ApplicationItem &item) const;
private:
    QString m_applicationName;
    QString m_applicationIcon;
    QString m_applicationDesktopFile;
    ApplicationCategory m_applicationCategory;
};

class AppFilterModel : public QSortFilterProxyModel
{
    Q_OBJECT
    Q_PROPERTY(bool showOnlyPreferredApps READ showOnlyPreferredApps WRITE setShowOnlyPrefferedApps)
    Q_PROPERTY(QString filter READ filter WRITE setFilter)
public:
    explicit AppFilterModel(QObject *parent = nullptr);
    ~AppFilterModel() override;

    void setShowOnlyPrefferedApps(bool show);
    bool showOnlyPreferredApps() const;

    void setFilter(const QString &text);
    QString filter() const;

protected:
    bool filterAcceptsRow(int source_row, const QModelIndex &source_parent) const override;
    bool lessThan(const QModelIndex &left, const QModelIndex &right) const override;

private:
    bool m_showOnlyPreferredApps = true;
    QString m_filter;
};

class AppModel : public QAbstractListModel
{
    Q_OBJECT
public:
    enum ItemRoles {
        ApplicationNameRole = Qt::UserRole + 1,
        ApplicationIconRole,
        ApplicationDesktopFileRole,
        ApplicationCategoryRole
    };

    explicit AppModel(QObject *parent = nullptr);
    ~AppModel() override;

    void setPreferredApps(const QStringList &list);

    QVariant data(const QModelIndex &index, int role) const override;
    int rowCount(const QModelIndex &parent) const override;
    QHash<int, QByteArray> roleNames() const override;

private:
    void loadApplications();

    QList<ApplicationItem> m_list;
};

#endif // APPMODEL_H
