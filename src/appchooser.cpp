/*
 * Copyright © 2016-2018 Red Hat, Inc
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see <http://www.gnu.org/licenses/>.
 *
 * Authors:
 *       Jan Grulich <jgrulich@redhat.com>
 */

#include "appchooser.h"
#include "appchooserdialog.h"
#include "utils.h"

#include <QLoggingCategory>
#include <QGuiApplication>

Q_LOGGING_CATEGORY(XdgDesktopPortalKirigamiAppChooser, "xdp-kde-app-chooser")

AppChooserPortal::AppChooserPortal(QObject *parent)
    : QDBusAbstractAdaptor(parent)
{
}

AppChooserPortal::~AppChooserPortal()
{
}

uint AppChooserPortal::ChooseApplication(const QDBusObjectPath &handle,
                                         const QString &app_id,
                                         const QString &parent_window,
                                         const QStringList &choices,
                                         const QVariantMap &options,
                                         QVariantMap &results)
{
    qCDebug(XdgDesktopPortalKirigamiAppChooser) << "ChooseApplication called with parameters:";
    qCDebug(XdgDesktopPortalKirigamiAppChooser) << "    handle: " << handle.path();
    qCDebug(XdgDesktopPortalKirigamiAppChooser) << "    app_id: " << app_id;
    qCDebug(XdgDesktopPortalKirigamiAppChooser) << "    parent_window: " << parent_window;
    qCDebug(XdgDesktopPortalKirigamiAppChooser) << "    choices: " << choices;
    qCDebug(XdgDesktopPortalKirigamiAppChooser) << "    options: " << options;

    QString latestChoice;

    if (options.contains(QStringLiteral("last_choice"))) {
        latestChoice = options.value(QStringLiteral("last_choice")).toString();
    }

    AppChooserDialog *appDialog = new AppChooserDialog(choices, latestChoice, options.value(QStringLiteral("filename")).toString());
    m_appChooserDialogs.insert(handle.path(), appDialog);

    bool handled = false;
    connect(appDialog, &AppChooserDialog::accepted, this, [&] (const QString &desktopFile) {
        results.insert(QStringLiteral("choice"), desktopFile);

        handled = true;
    });


    while (!handled)
        QGuiApplication::instance()->processEvents();

    m_appChooserDialogs.remove(handle.path());
    appDialog->deleteLater();

    return 0;
}

void AppChooserPortal::UpdateChoices(const QDBusObjectPath &handle, const QStringList &choices)
{
    qCDebug(XdgDesktopPortalKirigamiAppChooser) << "UpdateChoices called with parameters:";
    qCDebug(XdgDesktopPortalKirigamiAppChooser) << "    handle: " << handle.path();
    qCDebug(XdgDesktopPortalKirigamiAppChooser) << "    choices: " << choices;

    if (m_appChooserDialogs.contains(handle.path())) {
        m_appChooserDialogs.value(handle.path())->updateChoices(choices);
    }
}
